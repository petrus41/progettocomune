// Fill out your copyright notice in the Description page of Project Settings.
#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "PlayerControllerInterface.h"
#include "PlayerControllerBase.generated.h"

/**
 *
 */
UCLASS()
class BIOCRISIS_API APlayerControllerBase : public APlayerController, public IPlayerControllerInterface
{
	GENERATED_BODY()
public:
	APlayerControllerBase();
	virtual FInputSignature* GetBaseAttackDelegate() override;
	virtual FInputSignature* GetChargedAttackDelegate() override;
	virtual FInputSignature* GetSwitchStateDelegate() override;
protected:
	virtual void SetupInputComponent() override;
	void MoveForward(float AxisValue);
	void MoveRight(float AxisValue);
	void PlayerTick(float DeltaTime) override;
	void UseAbility1();
	void StartTimer();
	void StopTimer();
	void Charged();
	UPROPERTY(EditDefaultsOnly)
		float ChargeTimer;
	bool bIsCharged;
	FTimerHandle ChargeAttackTimer;
	FInputSignature BaseAttackDelegate;
	FInputSignature ChargedAttackDelegate;
	FInputSignature SwitchStateDelegate;

};

