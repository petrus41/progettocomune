// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "../StateMachine/StateBase.h"
#include "EnemyStateBase.generated.h"

/**
 *
 */
UCLASS()
class BIOCRISIS_API UEnemyStateBase : public UStateBase
{
	GENERATED_BODY()
protected:
	class AEnemyAIController* OwnerActor;
	APawn* PlayerPawn;
	float Distance;
	virtual void OnStateEnter(AActor* Owner) override;
	virtual void StateTick() override;
};
