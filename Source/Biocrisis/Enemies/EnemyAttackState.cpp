// Fill out your copyright notice in the Description page of Project Settings.


#include "EnemyAttackState.h"
#include "EnemyAIController.h"
#include "EnemyBase.h"

void UEnemyAttackState::OnStateEnter(AActor* Owner)
{
	Super::OnStateEnter(Owner);
	AEnemyBase* ControlledEnemy = Cast<AEnemyBase>(OwnerActor->GetPawn());
	if (ControlledEnemy)
	{
		ControlledEnemy->Gun->PullTrigger();
		ControlledEnemy->OnExtraActionActivation.AddDynamic(this, &UEnemyAttackState::SwitchToExtraState);
		ControlledEnemy->OnDeath.AddDynamic(this, &UEnemyAttackState::SwitchToDead);
	}
}
void UEnemyAttackState::StateTick()
{
	Super::StateTick();
	if (Distance >= OwnerActor->AggroDistance)
	{
		AEnemyBase* ControlledEnemy = Cast<AEnemyBase>(OwnerActor->GetPawn());
		if (ControlledEnemy)
		{
			ControlledEnemy->Gun->ReleaseTrigger();
		}
		OwnerActor->Processor->SwitchStateByKey("Chase");
	}
}
void UEnemyAttackState::OnStateExit()
{
	Super::OnStateExit();
	AEnemyBase* ControlledEnemy = Cast<AEnemyBase>(OwnerActor->GetPawn());
	if (ControlledEnemy)
	{
		ControlledEnemy->OnExtraActionActivation.RemoveAll(this);
		ControlledEnemy->OnDeath.RemoveAll(this);
	}
}

void UEnemyAttackState::SwitchToExtraState()
{
	AEnemyBase* ControlledEnemy = Cast<AEnemyBase>(OwnerActor->GetPawn());
	if (ControlledEnemy)
	{
		ControlledEnemy->Gun->ReleaseTrigger();
	}
	OwnerActor->Processor->SwitchStateByKey(ExtraStateName);
}

void UEnemyAttackState::SwitchToDead(float Null)
{
	AEnemyBase* ControlledEnemy = Cast<AEnemyBase>(OwnerActor->GetPawn());
	if (ControlledEnemy)
	{
		ControlledEnemy->Gun->ReleaseTrigger();
	}
	OwnerActor->Processor->SwitchStateByKey("Dead");
}