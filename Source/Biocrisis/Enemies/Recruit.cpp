// Fill out your copyright notice in the Description page of Project Settings.



#include "Recruit.h"
#include "Components/SphereComponent.h"

ARecruit::ARecruit()
{
	Trigger = CreateDefaultSubobject<USphereComponent>(TEXT("Trigger"));
	Trigger->SetSphereRadius(500);
	Trigger->SetupAttachment(GetRootComponent());
}
void ARecruit::BeginPlay()
{
	Super::BeginPlay();
	Trigger->OnComponentBeginOverlap.AddDynamic(this, &ARecruit::OnOverlapBegin);
	Trigger->OnComponentEndOverlap.AddDynamic(this, &ARecruit::OnOverlapEnd);
}
void ARecruit::OnOverlapEnd(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	AEnemyBase* Check = Cast<AEnemyBase>(OtherActor);
	if (Check && Check != this)
	{
		Check->OnDeath.RemoveAll(this);
		//UE_LOG(LogTemp, Warning, TEXT(" Check Name Begin: %s"), *Check->GetName());
	}
}
void ARecruit::OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	AEnemyBase* Check = Cast<AEnemyBase>(OtherActor);
	if (Check && Check != this)
	{
		Check->OnDeath.AddDynamic(this, &ARecruit::StartPanic);
		UE_LOG(LogTemp, Warning, TEXT("Check Name Start: %s"), *Check->GetName());
	}
}
void ARecruit::StartPanic(float Null)
{
	OnExtraActionActivation.Broadcast();
}

