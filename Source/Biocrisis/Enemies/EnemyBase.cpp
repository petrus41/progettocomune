// Fill out your copyright notice in the Description page of Project Settings.


#include "EnemyBase.h"

// Sets default values
AEnemyBase::AEnemyBase()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AEnemyBase::BeginPlay()
{
	Super::BeginPlay();
	Health = MaxHealth;
	if (GunClass == nullptr)
		return;
	Gun = GetWorld()->SpawnActor<AGunBase>(GunClass);
	Gun->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetNotIncludingScale, TEXT("WeaponSocket"));
	Gun->SetOwner(this);
}
void AEnemyBase::SetActive(bool Active)
{
	SetActorHiddenInGame(!Active);
	SetActorEnableCollision(Active);
	Gun->SetActorEnableCollision(Active);
	Gun->SetActorHiddenInGame(Active);
	//SetActorTickEnabled(Active);
}
void AEnemyBase::Deactivate()
{
	SetActive(false);
}
void AEnemyBase::Stun(float Duration)
{
}
float AEnemyBase::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	float DamageToApply = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);

	DamageToApply = FMath::Min(Health, DamageToApply);

	Health -= DamageToApply;

	UE_LOG(LogTemp, Warning, TEXT("EnemyHealth: %f"), Health);

	if (Health <= 0)
	{
		OnDeath.Broadcast(PointsValue);
		SetActive(false);
	}

	return DamageToApply;
}




