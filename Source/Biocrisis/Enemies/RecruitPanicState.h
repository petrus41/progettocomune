// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "EnemyStateBase.h"
#include "RecruitPanicState.generated.h"

/**
 *
 */
UCLASS()
class BIOCRISIS_API URecruitPanicState : public UEnemyStateBase
{
	GENERATED_BODY()
protected:
	void Evaluation();
	UFUNCTION()
		void SwitchToDead(float Null);
	FTimerHandle PanicDurationTimer;
public:
	virtual void OnStateEnter(AActor* Owner) override;
	virtual void OnStateExit() override;
};
