// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "EnemyStateBase.h"
#include "EnemyChaseState.generated.h"

/**
 *
 */
UCLASS()
class BIOCRISIS_API UEnemyChaseState : public UEnemyStateBase
{
	GENERATED_BODY()
protected:
	UPROPERTY(EditDefaultsOnly)
		FString ExtraStateName;
	UFUNCTION()
		void SwitchToExtraState();
	UFUNCTION()
		void SwitchToDead(float Null);
public:
	virtual void OnStateEnter(AActor* Owner);
	virtual void OnStateExit();
	virtual void StateTick();
};
