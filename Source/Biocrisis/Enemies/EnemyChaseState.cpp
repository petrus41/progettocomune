// Fill out your copyright notice in the Description page of Project Settings.


#include "EnemyChaseState.h"
#include "EnemyAIController.h"
#include "EnemyBase.h"

void UEnemyChaseState::OnStateEnter(AActor* Owner)
{
	Super::OnStateEnter(Owner);
	AEnemyBase* ControlledEnemy = Cast<AEnemyBase>(OwnerActor->GetPawn());
	if (ControlledEnemy)
	{
		ControlledEnemy->OnExtraActionActivation.AddDynamic(this, &UEnemyChaseState::SwitchToExtraState);
		ControlledEnemy->OnDeath.AddDynamic(this, &UEnemyChaseState::SwitchToDead);
	}
	OwnerActor->MoveToActor(PlayerPawn);
}


void UEnemyChaseState::StateTick()
{
	Super::StateTick();
	if (Distance < OwnerActor->AggroDistance * 0.66)
	{
		OwnerActor->Processor->SwitchStateByKey("Attack");
	}
}

void UEnemyChaseState::OnStateExit()
{
	Super::OnStateExit();
	OwnerActor->StopMovement();
	AEnemyBase* ControlledEnemy = Cast<AEnemyBase>(OwnerActor->GetPawn());
	if (ControlledEnemy)
	{
		ControlledEnemy->OnExtraActionActivation.RemoveAll(this);
		ControlledEnemy->OnDeath.RemoveAll(this);
	}
}

void UEnemyChaseState::SwitchToExtraState()
{
	OwnerActor->Processor->SwitchStateByKey(ExtraStateName);
}

void UEnemyChaseState::SwitchToDead(float Null)
{
	OwnerActor->Processor->SwitchStateByKey("Dead");
}
