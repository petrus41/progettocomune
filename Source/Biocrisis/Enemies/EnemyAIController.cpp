// Fill out your copyright notice in the Description page of Project Settings.


#include "EnemyAIController.h"

void AEnemyAIController::BeginPlay()
{
	Super::BeginPlay();
	Processor->InitStateManger();
}

AEnemyAIController::AEnemyAIController()
{
	Processor = CreateDefaultSubobject<UStateMachineComponent>(TEXT("Processor"));
}