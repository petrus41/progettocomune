// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "EnemyStateBase.h"
#include "EnemyDeadState.generated.h"

/**
 *
 */
UCLASS()
class BIOCRISIS_API UEnemyDeadState : public UEnemyStateBase
{
	GENERATED_BODY()
public:
	void OnStateEnter(AActor* Owner) override;
};
