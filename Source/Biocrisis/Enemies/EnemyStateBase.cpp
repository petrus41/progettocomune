// Fill out your copyright notice in the Description page of Project Settings.


#include "EnemyStateBase.h"
#include "EnemyAIController.h"
#include "Kismet/GameplayStatics.h"

void UEnemyStateBase::OnStateEnter(AActor* Owner)
{
	Super::OnStateEnter(Owner);
	//UE_LOG(LogTemp, Warning, TEXT("%s"), *this->GetName());
	if (!OwnerActor)
	{
		OwnerActor = Cast<AEnemyAIController>(Owner);
	}
	if (!PlayerPawn)
	{
		PlayerPawn = UGameplayStatics::GetPlayerPawn(OwnerActor->GetWorld(), 0);
	}
	OwnerActor->SetFocus(PlayerPawn);
}
void UEnemyStateBase::StateTick()
{
	Super::StateTick();
	FVector EnemyPosition = OwnerActor->GetPawn()->GetActorLocation();
	FVector PlayerPosition = PlayerPawn->GetActorLocation();
	Distance = (EnemyPosition - PlayerPosition).Length();
}


