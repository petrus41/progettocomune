// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "../GunBase.h"
#include "../ObjectPool/PoolableObject.h"
#include "EnemyBase.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FDeathSignature, float, ExperiencePoints);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FExtraAction);
UCLASS()
class BIOCRISIS_API AEnemyBase : public ACharacter, public IPoolableObject
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AEnemyBase();
	AGunBase* Gun;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	UPROPERTY(EditAnyWhere)
		TSubclassOf<class AGunBase> GunClass;
	virtual void SetActive(bool Active) override;
	virtual void Deactivate() override;
public:
	FExtraAction OnExtraActionActivation;
	FDeathSignature OnDeath;
	virtual float TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) override;

	virtual void Stun(float Duration);
	UPROPERTY(EditAnywhere)
		float MaxHealth = 100;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		float Health;

	UPROPERTY(EditAnywhere)
		float PointsValue = 2.f;

};

