// Fill out your copyright notice in the Description page of Project Settings.

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "EnemyBase.h"
#include "Recruit.generated.h"

/**
 *
 */
UCLASS()
class BIOCRISIS_API ARecruit : public AEnemyBase
{
	GENERATED_BODY()
public:
	ARecruit();
protected:
	UPROPERTY(EditDefaultsOnly)
		class USphereComponent* Trigger;

	UFUNCTION()
		void OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	UFUNCTION()
		void OnOverlapEnd(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);
	UFUNCTION()
		void StartPanic(float Null);
	virtual void BeginPlay() override;
};
