// Fill out your copyright notice in the Description page of Project Settings.


#include "RecruitPanicState.h"
#include "EnemyAIController.h"
#include "EnemyBase.h"

void URecruitPanicState::OnStateEnter(AActor* Owner)
{
	Super::OnStateEnter(Owner);
	OwnerActor->GetWorldTimerManager().SetTimer(PanicDurationTimer, this, &URecruitPanicState::Evaluation, 3);
	AEnemyBase* ControlledEnemy = Cast<AEnemyBase>(OwnerActor->GetPawn());
	if (ControlledEnemy)
	{
		ControlledEnemy->OnDeath.AddDynamic(this, &URecruitPanicState::SwitchToDead);
	}
}
void URecruitPanicState::Evaluation()
{
	AEnemyBase* ControlledEnemy = Cast<AEnemyBase>(OwnerActor->GetPawn());
	if (ControlledEnemy)
	{
		ControlledEnemy->OnDeath.RemoveAll(this);
	}
	if (Distance < OwnerActor->AggroDistance)
	{
		OwnerActor->Processor->SwitchStateByKey("Attack");
	}
	else
	{
		OwnerActor->Processor->SwitchStateByKey("Chase");
	}
}

void URecruitPanicState::OnStateExit()
{
	OwnerActor->GetWorldTimerManager().ClearTimer(PanicDurationTimer);
}

void URecruitPanicState::SwitchToDead(float Null)
{
	OwnerActor->Processor->SwitchStateByKey("Dead");
}