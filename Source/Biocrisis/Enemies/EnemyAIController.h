// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "../StateMachine/StateMachineComponent.h"
#include "EnemyAIController.generated.h"

/**
 *
 */
UCLASS()
class BIOCRISIS_API AEnemyAIController : public AAIController
{
	GENERATED_BODY()
protected:
	void BeginPlay() override;
public:
	AEnemyAIController();
	UPROPERTY(EditDefaultsOnly)
		float AggroDistance;
	UPROPERTY(VisibleAnywhere)
		class UStateMachineComponent* Processor;
};