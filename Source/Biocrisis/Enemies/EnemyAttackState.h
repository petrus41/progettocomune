// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "EnemyStateBase.h"
#include "EnemyAttackState.generated.h"

/**
 *
 */
UCLASS()
class BIOCRISIS_API UEnemyAttackState : public UEnemyStateBase
{
	GENERATED_BODY()
protected:
	UPROPERTY(EditDefaultsOnly)
		FString ExtraStateName;
	UFUNCTION()
		void SwitchToExtraState();
	UFUNCTION()
		void SwitchToDead(float Null);
public:
	virtual void OnStateEnter(AActor* Owner) override;
	virtual void OnStateExit() override;
	virtual void StateTick() override;
};

