// Fill out your copyright notice in the Description page of Project Settings.


#include "EnemyIdleState.h"
#include "EnemyAIController.h"

void UEnemyIdleState::OnStateEnter(AActor* Owner)
{
	Super::OnStateEnter( Owner);
	FVector EnemyPosition = OwnerActor->GetPawn()->GetActorLocation();
	FVector PlayerPosition = PlayerPawn->GetActorLocation();
	Distance = (EnemyPosition - PlayerPosition).Length();
	if (Distance > OwnerActor->AggroDistance * 0, 66)
		OwnerActor->Processor->SwitchStateByKey("Chase");
	else
		OwnerActor->Processor->SwitchStateByKey("Attack");
}