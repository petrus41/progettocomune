// Fill out your copyright notice in the Description page of Project Settings.


#pragma once

#include "CoreMinimal.h"
#include "EnemyStateBase.h"
#include "EnemyIdleState.generated.h"

/**
 *
 */
UCLASS()
class BIOCRISIS_API UEnemyIdleState : public UEnemyStateBase
{
	GENERATED_BODY()
public:
	virtual void OnStateEnter(AActor* Owner) override;
};

