// Fill out your copyright notice in the Description page of Project Settings.


#include "ProjectileBase.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Components/StaticMeshComponent.h"
#include "PlayerCharacter.h"
#include "Enemies/EnemyBase.h"

// Sets default values
AProjectileBase::AProjectileBase()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	RootComponent = Mesh;

	MovementComponent = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileMovementComponent"));
	MovementComponent->SetUpdatedComponent(Mesh);
	MovementComponent->InitialSpeed = 320.0f;
	MovementComponent->ProjectileGravityScale = 0.f;
	MovementComponent->bRotationFollowsVelocity = true;
	MovementComponent->MaxSpeed = 2000.f;
}

void AProjectileBase::BeginPlay()
{
	Super::BeginPlay();
	MovementComponent->OnProjectileStop.AddDynamic(this, &AProjectileBase::OnProjectileStop);
	Mesh->OnComponentBeginOverlap.AddDynamic(this, &AProjectileBase::OnOverlapBegin);
}

void AProjectileBase::Deactivate()
{
	SetActive(false);
}

void AProjectileBase::OnProjectileStop(const FHitResult& ImpactResult)
{
	MovementComponent->SetUpdatedComponent(Mesh);
}
void AProjectileBase::SetActive(bool Active)
{
	SetActorHiddenInGame(!Active);
	SetActorEnableCollision(Active);
	SetActorTickEnabled(Active);
	bIsActive = Active;
	if (Active)
		GetWorldTimerManager().SetTimer(LifeSpanTimer, this, &AProjectileBase::Deactivate, LifeSpan, false);
}
void AProjectileBase::SetUp(FVector Direction, float Speed)
{
	MovementComponent->Velocity = Direction * Speed;
}
void AProjectileBase::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (!Cast<AController>(GetOwner())->IsPlayerController())
	{
		APlayerCharacter* Player = Cast<APlayerCharacter>(OtherActor);
		if (Player)
		{
			Player->TakeDamage(Damage, FDamageEvent(UDamageType::StaticClass()), Cast<AController>(GetOwner()), this);
			SetActive(false);
		}
	}
	else
	{
		AEnemyBase* Enemy = Cast<AEnemyBase>(OtherActor);
		if (Enemy)
		{
			Enemy->TakeDamage(Damage, FDamageEvent(UDamageType::StaticClass()), Cast<AController>(GetOwner()), this);
			SetActive(false);
		}
	}


}

