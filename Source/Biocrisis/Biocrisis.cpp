// Copyright Epic Games, Inc. All Rights Reserved.

#include "Biocrisis.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, Biocrisis, "Biocrisis" );
