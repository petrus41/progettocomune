// Fill out your copyright notice in the Description page of Project Settings.


#include "GunStateBase.h"



void UGunStateBase::OnStateEnter(AActor* Owner)
{
	Super::OnStateEnter(Owner);
	if (!OwnerActor)
	{
		OwnerActor = Cast<AGunBase>(Owner);
	}
}