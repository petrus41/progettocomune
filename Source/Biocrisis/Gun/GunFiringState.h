// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GunStateBase.h"
#include "GunFiringState.generated.h"

/**
 *
 */
UCLASS()
class BIOCRISIS_API UGunFiringState : public UGunStateBase
{
	GENERATED_BODY()
protected:
	void PullTrigger();
	FTimerHandle FireRateTimer;
	AController* ProjectileOwner;
public:
	virtual void OnStateEnter(AActor* Owner) override;
	virtual void StateTick() override;
};