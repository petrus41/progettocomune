// Fill out your copyright notice in the Description page of Project Settings.


#include "GunReloadState.h"

void UGunReloadState::Evaluation()
{
	OwnerActor->Reload();
	if (OwnerActor->bIsFireing)
	{
		OwnerActor->Processor->SwitchStateByKey("Firing");
	}
	else
	{
		OwnerActor->Processor->SwitchStateByKey("Idle");
	}
}

void UGunReloadState::OnStateEnter(AActor* Owner)
{
	Super::OnStateEnter(Owner);
	FTimerHandle ReloadTimer;
	OwnerActor->GetWorldTimerManager().SetTimer(ReloadTimer, this, &UGunReloadState::Evaluation, OwnerActor->ReloadTime);
}