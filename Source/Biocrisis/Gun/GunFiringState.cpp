// Fill out your copyright notice in the Description page of Project Settings.


#include "GunFiringState.h"
#include "GameFramework/Pawn.h"

void UGunFiringState::PullTrigger()
{
	if (OwnerActor->bIsFireing && OwnerActor->AmmoInMag > 0)
	{
		OwnerActor->SpawnProjectile(ProjectileOwner, OwnerActor->Damage);
		OwnerActor->GetWorldTimerManager().SetTimer(FireRateTimer, this, &UGunFiringState::PullTrigger, OwnerActor->FireRate);
	}
}
void UGunFiringState::OnStateEnter(AActor* Owner)
{
	Super::OnStateEnter(Owner);
	ProjectileOwner = Cast<APawn>(OwnerActor->GetOwner())->GetController();
	if (!OwnerActor->GetWorldTimerManager().IsTimerActive(FireRateTimer))
	{
		OwnerActor->GetWorldTimerManager().SetTimer(FireRateTimer, this, &UGunFiringState::PullTrigger, OwnerActor->FireRate, false, 0);
	}
}
void UGunFiringState::StateTick()
{
	if (OwnerActor->AmmoInMag <= 0)
	{
		OwnerActor->Processor->SwitchStateByKey("Reload");
	}
	else if (!OwnerActor->bIsFireing)
	{
		OwnerActor->Processor->SwitchStateByKey("Idle");
	}
}