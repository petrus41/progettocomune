// Fill out your copyright notice in the Description page of Project Settings.


#include "GunIdleState.h"


void UGunIdleState::StateTick()
{
	Super::StateTick();
	if (OwnerActor->bIsFireing)
	{
		OwnerActor->Processor->SwitchStateByKey("Firing");
	}
}