// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GunStateBase.h"
#include "GunIdleState.generated.h"

/**
 *
 */
UCLASS()
class BIOCRISIS_API UGunIdleState : public UGunStateBase
{
	GENERATED_BODY()
public:
	virtual void StateTick() override;
};
