// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GunStateBase.h"
#include "GunReloadState.generated.h"

/**
 *
 */
UCLASS()
class BIOCRISIS_API UGunReloadState : public UGunStateBase
{
	GENERATED_BODY()
protected:
	void Evaluation();
public:
	virtual void OnStateEnter(AActor* Owner) override;
};

