// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "../StateMachine/StateBase.h"
#include "../GunBase.h"
#include "../StateMachine/StateMachineComponent.h"
#include "GunStateBase.generated.h"

/**
 *
 */
UCLASS()
class BIOCRISIS_API UGunStateBase : public UStateBase
{
	GENERATED_BODY()
protected:
	AGunBase* OwnerActor;
	virtual void OnStateEnter(AActor* Owner) override;
};
