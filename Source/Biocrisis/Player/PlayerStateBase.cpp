// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerStateBase.h"
#include "../PlayerCharacter.h"
#include "../PlayerControllerInterface.h"

void UPlayerStateBase::OnStateEnter(AActor* Owner)
{
	Super::OnStateEnter(Owner);
	if (!Player)
	{
		Player = Cast<APlayerCharacter>(Owner);
	}
	if (!PlayerController && Player)
	{
		PlayerController = Cast<IPlayerControllerInterface>(Player->GetController());
	}
	if (PlayerController)
	{
		PlayerController->GetBaseAttackDelegate()->AddDynamic(this, &UPlayerStateBase::BaseAttack);
		PlayerController->GetChargedAttackDelegate()->AddDynamic(this, &UPlayerStateBase::ChargedAttack);
		PlayerController->GetSwitchStateDelegate()->AddDynamic(this, &UPlayerStateBase::SwitchState);
	}
}

void UPlayerStateBase::OnStateExit()
{
	Super::OnStateExit();
	if (PlayerController)
	{
		PlayerController->GetBaseAttackDelegate()->RemoveAll(this);
		PlayerController->GetChargedAttackDelegate()->RemoveAll(this);
		PlayerController->GetSwitchStateDelegate()->RemoveAll(this);
	}
}

void UPlayerStateBase::BaseAttack()
{
	//GEngine->AddOnScreenDebugMessage(-1, 6, FColor::Red, TEXT("Base"));
}

void UPlayerStateBase::ChargedAttack()
{
	//GEngine->AddOnScreenDebugMessage(-1, 6, FColor::Red, TEXT(" Charged"));
}

void UPlayerStateBase::SwitchState()
{

}

