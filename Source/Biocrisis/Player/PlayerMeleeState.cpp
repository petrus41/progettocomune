// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerMeleeState.h"
#include "../PlayerCharacter.h"
#include "../StateMachine/StateMachineComponent.h"
#include "../Ability/AbilityManagerComponent.h"

void UPlayerMeleeState::BaseAttack()
{
	Super::BaseAttack();
	if (Player->AbilityManager->GetAbilityByName("BaseMelee"))
	{
		Player->AbilityManager->GetAbilityByName("BaseMelee")->UseAbility();
	}
}
void UPlayerMeleeState::SwitchState()
{
	Super::SwitchState();
	Player->Processor->SwitchStateByKey("Ranged");
}
void UPlayerMeleeState::ChargedAttack()
{
	Super::ChargedAttack();
	if (Player->AbilityManager->GetAbilityByName("ChargedMelee"))
	{
		Player->AbilityManager->GetAbilityByName("ChargedMelee")->UseAbility();
	}
}