// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PlayerStateBase.h"
#include "PlayerMeleeState.generated.h"

/**
 * 
 */

UCLASS()
class BIOCRISIS_API UPlayerMeleeState : public UPlayerStateBase
{
	GENERATED_BODY()
protected:
	virtual void BaseAttack() override;
	virtual void SwitchState() override;
	virtual void ChargedAttack() override;
};
