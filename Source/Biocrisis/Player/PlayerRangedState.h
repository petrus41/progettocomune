// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PlayerStateBase.h"
#include "PlayerRangedState.generated.h"

/**
 * 
 */
UCLASS()
class BIOCRISIS_API UPlayerRangedState : public UPlayerStateBase
{
	GENERATED_BODY()
protected:
	virtual void BaseAttack() override;
	virtual void SwitchState() override;
	virtual void ChargedAttack() override;
	
};
