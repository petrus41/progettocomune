// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerRangedState.h"
#include "../PlayerCharacter.h"
#include "../StateMachine/StateMachineComponent.h"
#include "../Ability/AbilityManagerComponent.h"

void UPlayerRangedState::BaseAttack()
{
	Super::BaseAttack();
	if (Player->AbilityManager->GetAbilityByName("BaseRanged"))
	{
		Player->AbilityManager->GetAbilityByName("BaseRanged")->UseAbility();
	}
}
void UPlayerRangedState::SwitchState()
{
	Super::SwitchState();
	Player->Processor->SwitchStateByKey("Melee");
}
void UPlayerRangedState::ChargedAttack()
{
	Super::ChargedAttack();
	if (Player->AbilityManager->GetAbilityByName("ChargedRanged"))
	{
		Player->AbilityManager->GetAbilityByName("ChargedRanged")->UseAbility();
	}
}