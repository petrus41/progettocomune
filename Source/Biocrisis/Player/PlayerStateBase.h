// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "../StateMachine/StateBase.h"
#include "PlayerStateBase.generated.h"

/**
 *
 */
UCLASS()
class BIOCRISIS_API UPlayerStateBase : public UStateBase
{
	GENERATED_BODY()
protected:
	class APlayerCharacter* Player;
	class IPlayerControllerInterface* PlayerController;
	UFUNCTION()
		virtual void BaseAttack();
	UFUNCTION()
		virtual void ChargedAttack();
	UFUNCTION()
		virtual void SwitchState();
public:
	virtual void OnStateEnter(AActor* Owner) override;
	virtual void OnStateExit() override;

};
