// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "BiocrisisGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class BIOCRISIS_API ABiocrisisGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
