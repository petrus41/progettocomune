// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerCharacter.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "StateMachine/StateMachineComponent.h"
#include "Ability/AbilityManagerComponent.h"

// Sets default values
APlayerCharacter::APlayerCharacter()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	GetCharacterMovement()->RotationRate = FRotator(0.f, RotRate, 0.f);

	SpringArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringArm"));
	SpringArm->SetupAttachment(RootComponent);
	SpringArm->TargetArmLength = 1500;
	SpringArm->SetWorldRotation(FRotator(-60.f, 0.f, 0.f));
	SpringArm->bDoCollisionTest = false;

	Camera = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	Camera->SetupAttachment(SpringArm, USpringArmComponent::SocketName);
	Camera->ProjectionMode = ECameraProjectionMode::Perspective;
	Camera->bUsePawnControlRotation = false;

	Processor = CreateDefaultSubobject<UStateMachineComponent>(TEXT("PlayerProcessor"));
	AbilityManager = CreateDefaultSubobject<UAbilityManagerComponent>(TEXT("Ability Manager"));
}

// Called when the game starts or when spawned
void APlayerCharacter::BeginPlay()
{
	Super::BeginPlay();
	Processor->InitStateManger();
	//AbilityManager->Ini
	CurrentHealth = MaxHealth;
}

float APlayerCharacter::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	float DamageToApply = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);
	DamageToApply = FMath::Min(CurrentHealth, DamageToApply);

	CurrentHealth -= DamageToApply;

	return DamageToApply;
}

void APlayerCharacter::UseAbility1()
{
	AbilityManager->GetAbilityByName("BaseMeleeAttack")->UseAbility();
}


