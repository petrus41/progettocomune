

// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerControllerBase.h"

APlayerControllerBase::APlayerControllerBase()
{
	DefaultMouseCursor = EMouseCursor::Default;
}

FInputSignature* APlayerControllerBase::GetBaseAttackDelegate()
{

	return &BaseAttackDelegate;
}

FInputSignature* APlayerControllerBase::GetChargedAttackDelegate()
{

	return &ChargedAttackDelegate;
}

FInputSignature* APlayerControllerBase::GetSwitchStateDelegate()
{
	return &SwitchStateDelegate;
}

void  APlayerControllerBase::SetupInputComponent()
{
	Super::SetupInputComponent();
	InputComponent->BindAxis(TEXT("MoveForward"), this, &APlayerControllerBase::MoveForward);
	InputComponent->BindAxis(TEXT("MoveRight"), this, &APlayerControllerBase::MoveRight);
	InputComponent->BindAction(TEXT("BaseAttack"), IE_Pressed, this, &APlayerControllerBase::StartTimer);
	InputComponent->BindAction(TEXT("BaseAttack"), IE_Released, this, &APlayerControllerBase::StopTimer);
	InputComponent->BindAction(TEXT("SwitchState"), IE_Released, this, &APlayerControllerBase::UseAbility1);
}

void APlayerControllerBase::StartTimer()
{
	GetWorldTimerManager().SetTimer(ChargeAttackTimer, this, &APlayerControllerBase::Charged, ChargeTimer);
}
void APlayerControllerBase::StopTimer()
{
	if (!bIsCharged)
	{
		GetWorldTimerManager().ClearTimer(ChargeAttackTimer);
		BaseAttackDelegate.Broadcast();
	}
	else
	{
		ChargedAttackDelegate.Broadcast();
	}
	bIsCharged = false;
}
void APlayerControllerBase::Charged()
{
	bIsCharged = true;
}
void APlayerControllerBase::PlayerTick(float DeltaTime)
{
	Super::PlayerTick(DeltaTime);

	FVector HitLocation = FVector::ZeroVector;
	FHitResult Hit;
	GetHitResultUnderCursor(ECC_Visibility, true, Hit);
	HitLocation = FVector(Hit.Location.X, Hit.Location.Y, 0);

	// Direct the Pawn towards that location
	APawn* const MyPawn = GetPawn();
	if (MyPawn)
	{
		FVector WorldDirection = (HitLocation - (MyPawn->GetActorLocation() * FVector(1, 1, 0))).GetSafeNormal();
		MyPawn->SetActorRotation(WorldDirection.Rotation());
	}
}
void APlayerControllerBase::MoveForward(float AxisValue)
{
	APawn* const MyPawn = GetPawn();
	if (MyPawn)
	{
		MyPawn->AddMovementInput(FVector(AxisValue, 0, 0).GetSafeNormal());
	}
}

void APlayerControllerBase::MoveRight(float AxisValue)
{
	APawn* const MyPawn = GetPawn();
	if (MyPawn)
	{
		MyPawn->AddMovementInput(FVector(0, AxisValue, 0).GetSafeNormal());
	}
}
void APlayerControllerBase::UseAbility1()
{
	if (SwitchStateDelegate.IsBound())
	{
		SwitchStateDelegate.Broadcast();
	}
}