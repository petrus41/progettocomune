// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AbilityBase.h"
#include "BaseRangedAttack.generated.h"

/**
 * 
 */
UCLASS()
class BIOCRISIS_API UBaseRangedAttack : public UAbilityBase
{
	GENERATED_BODY()
	
};
