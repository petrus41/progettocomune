// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "AbilityBase.generated.h"

/**
 *
 */
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FAbilityUseSignature);
UCLASS(Blueprintable)
class BIOCRISIS_API UAbilityBase : public UObject
{
	GENERATED_BODY()
public:
	virtual void Inizialize(APawn* OwnerCharacter);
	virtual void UseAbility();
	bool bIsUnlocked;
	FAbilityUseSignature OnAbilityStart;
	FAbilityUseSignature OnAbilityEnd;
protected:
	UPROPERTY(EditDefaultsOnly)
		int PointsToUnlock;
	UPROPERTY(EditDefaultsOnly)
		FName Name;
	UPROPERTY(EditDefaultsOnly)
		int Precision;
	UPROPERTY(EditDefaultsOnly)
		float Damage;
	TArray<AActor*> Cone(float Degrees, float Range);

	APawn* Owner;
};

