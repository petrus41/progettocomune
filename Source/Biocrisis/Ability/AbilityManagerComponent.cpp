// Fill out your copyright notice in the Description page of Project Settings.


#include "AbilityManagerComponent.h"

// Sets default values for this component's properties
UAbilityManagerComponent::UAbilityManagerComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UAbilityManagerComponent::BeginPlay()
{
	Super::BeginPlay();

	InizializeAbility();

}


// Called every frame
void UAbilityManagerComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UAbilityManagerComponent::InizializeAbility()
{
	for (const TPair<FString, TSubclassOf<UAbilityBase>> pair : PossibleAbility)
	{
		UAbilityBase* Ability = NewObject<UAbilityBase>(this, pair.Value);
		ActiveAbility.Add(pair.Key, Ability);
		Ability->Inizialize(Cast<APawn>(GetOwner()));
	}
}
UAbilityBase* UAbilityManagerComponent::GetAbilityByName(FString Name)
{
	return ActiveAbility.FindRef(Name);
}