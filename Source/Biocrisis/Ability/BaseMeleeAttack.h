// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AbilityBase.h"
#include "BaseMeleeAttack.generated.h"

/**
 *
 */
UCLASS()
class BIOCRISIS_API UBaseMeleeAttack : public UAbilityBase
{
	GENERATED_BODY()
public:
	virtual void UseAbility() override;
protected:
	UPROPERTY(EditDefaultsOnly)
		float Range;
	UPROPERTY(EditDefaultsOnly)
		float Degrees;

};
