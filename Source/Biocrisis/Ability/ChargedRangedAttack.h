// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AbilityBase.h"
#include "ChargedRangedAttack.generated.h"

/**
 * 
 */
UCLASS()
class BIOCRISIS_API UChargedRangedAttack : public UAbilityBase
{
	GENERATED_BODY()
public:
	virtual void UseAbility() override;
};
