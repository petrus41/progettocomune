// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "AbilityBase.h"
#include "AbilityManagerComponent.generated.h"


UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class BIOCRISIS_API UAbilityManagerComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UAbilityManagerComponent();

	UAbilityBase* GetAbilityByName(FString Name);

protected:
	// Called when the game starts
	virtual void BeginPlay() override;
	UPROPERTY(EditDefaultsOnly)
		TMap<FString, TSubclassOf<UAbilityBase>> PossibleAbility;
	UPROPERTY(BlueprintReadOnly)
		TMap<FString, UAbilityBase*> ActiveAbility;
	void InizializeAbility();

public:
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;


};

