// Fill out your copyright notice in the Description page of Project Settings.


#include "BaseMeleeAttack.h"

void UBaseMeleeAttack::UseAbility()
{
	for (AActor* Hitted : Cone(Degrees, Range))
	{
		Hitted->TakeDamage(Damage, FDamageEvent(UDamageType::StaticClass()), Owner->GetController(), Owner);
	}
}