// Fill out your copyright notice in the Description page of Project Settings.


#include "AbilityBase.h"
#include "GameFramework/Character.h"
#include "DrawDebugHelpers.h"

void UAbilityBase::UseAbility()
{


}
TArray<AActor*> UAbilityBase::Cone(float Degrees, float Range)
{
	TArray<AActor*> ReturnActors = TArray<AActor*>();
	float DivisionDegrees = Degrees / Precision;
	FVector Start = Owner->GetActorLocation();
	for (int i = -Precision / 2; i < Precision / 2; i++)
	{
		FVector End = Owner->GetActorForwardVector().RotateAngleAxis(i * DivisionDegrees, FVector(0, 0, 1)) * Range + Start;
		TArray<FHitResult> Hit = TArray<FHitResult>();
		Owner->GetWorld()->LineTraceMultiByChannel(Hit, Start, End, ECollisionChannel::ECC_Visibility);
		DrawDebugLine(Owner->GetWorld(), Start, End, FColor::Red, true);
		for (FHitResult HitResult : Hit)
		{
			ReturnActors.AddUnique(HitResult.GetActor());
		}
	}
	return ReturnActors;
}
void UAbilityBase::Inizialize(APawn* OwnerCharacter)
{
	Owner = OwnerCharacter;
}