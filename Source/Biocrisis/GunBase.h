// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "GunBase.generated.h"

UCLASS()
class BIOCRISIS_API AGunBase : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AGunBase();
	UPROPERTY(VisibleAnywhere)
		USceneComponent* Root;
	UPROPERTY(VisibleAnywhere)
		UStaticMeshComponent* Mesh;
	UPROPERTY(VisibleAnywhere, BluePrintReadOnly)
		class UStateMachineComponent* Processor;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	UPROPERTY(EditAnywhere)
		float ProjectileSpeed;
	UPROPERTY(EditAnywhere)
		int ClipSize;

	FTimerHandle FireRateTimer;

	FVector CalcolateInaccuracy();
	bool ConsumeAmmo();
public:

	UPROPERTY(EditAnywhere)
		float Damage = 10;
	UPROPERTY(EditDefaultsOnly)
		float FireRate;
	UPROPERTY(EditAnywhere)
		float ReloadTime;
	UPROPERTY(EditAnywhere)
		float Inaccuracy;
	int AmmoInMag;
	bool bIsFireing;
	class AObjectPoolActor* ObjectPool;

	void PullTrigger();
	void ReleaseTrigger();
	void Reload();
	virtual void SpawnProjectile(AController* OwnerController, float ProjectileDamage);
};

