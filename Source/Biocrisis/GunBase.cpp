// Fill out your copyright notice in the Description page of Project Settings.


#include "GunBase.h"
#include "StateMachine/StateMachineComponent.h"
#include "ObjectPool/ObjectPoolActor.h"
#include "Kismet/GameplayStatics.h"
#include "ProjectileBase.h"

// Sets default values
AGunBase::AGunBase()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	Root = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	SetRootComponent(Root);

	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	Mesh->SetupAttachment(Root);

	Processor = CreateDefaultSubobject<UStateMachineComponent>(TEXT("GunProcessor"));
}
void AGunBase::BeginPlay()
{
	Super::BeginPlay();
	Processor->InitStateManger();
	AActor* FoundActor = UGameplayStatics::GetActorOfClass(GetWorld(), AObjectPoolActor::StaticClass());
	if (!FoundActor)
		return;
	ObjectPool = Cast<AObjectPoolActor>(FoundActor);
}

void AGunBase::PullTrigger()
{
	bIsFireing = true;
}

void AGunBase::ReleaseTrigger()
{
	bIsFireing = false;
}

void AGunBase::SpawnProjectile(AController* OwnerController, float ProjectileDamage)
{
	if (ObjectPool == nullptr || !ConsumeAmmo())
		return;
	AActor* SpawnedActor = ObjectPool->GetAvailableActor();
	if (SpawnedActor)
	{
		SpawnedActor->SetOwner(OwnerController);
		IPoolableObject* PoolProjectile = Cast<IPoolableObject>(SpawnedActor);
		AProjectileBase* Projectile = Cast<AProjectileBase>(SpawnedActor);
		PoolProjectile->SetActive(true);
		Projectile->SetActorLocation(GetOwner()->GetActorLocation());
		Projectile->SetUp(CalcolateInaccuracy(), ProjectileSpeed);
		Projectile->Damage = Damage;
	}
}

FVector AGunBase::CalcolateInaccuracy()
{
	float ExtraAngle = FMath::RandRange(-Inaccuracy, Inaccuracy);
	FVector Direction = GetOwner()->GetActorForwardVector().RotateAngleAxis(ExtraAngle, FVector(0, 0, 1));
	return Direction;
}

void AGunBase::Reload()
{
	AmmoInMag = ClipSize;
}
bool AGunBase::ConsumeAmmo()
{
	if (AmmoInMag > 0)
	{
		AmmoInMag--;
		return true;
	}
	else
	{
		return false;
	}
}


