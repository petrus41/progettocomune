// Fill out your copyright notice in the Description page of Project Settings.


#include "ObjectPoolActor.h"
#include "PoolableObject.h"

// Sets default values
AObjectPoolActor::AObjectPoolActor()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AObjectPoolActor::BeginPlay()
{
	Super::BeginPlay();
	Initiazlize();
}

void AObjectPoolActor::Initiazlize()
{
	FVector Location(0.0f, 0.0f, 0.0f);
	FRotator Rotation(0.0f, 0.0f, 0.0f);
	FActorSpawnParameters SpawnInfo;
	if (PooledActorClass == nullptr)
		return;
	for (int i = 0; i < PoolSize; i++)
	{
		AActor* SpawnedActor = GetWorld()->SpawnActor<AActor>(PooledActorClass, Location, Rotation, SpawnInfo);
		IPoolableObject* ObjectToPool = Cast<IPoolableObject>(SpawnedActor);
		if (!ObjectToPool)
			return;
		Pool.Add(SpawnedActor);
		ObjectToPool->SetActive(false);
		ObjectToPool->LifeSpan = ActorLifeTime;
	}
}

AActor* AObjectPoolActor::GetAvailableActor()
{
	for (AActor* PooledActor : Pool)
	{
		IPoolableObject* ObjectInPool = Cast<IPoolableObject>(PooledActor);
		if (!ObjectInPool->bIsActive)
		{
			return PooledActor;
		}
	}
	Initiazlize();
	return GetAvailableActor();
}


