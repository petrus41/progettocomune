// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "PoolableObject.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UPoolableObject : public UInterface
{
	GENERATED_BODY()
};

/**
 *
 */
class BIOCRISIS_API IPoolableObject
{
	GENERATED_BODY()

		// Add interface functions to this class. This is the class that will be inherited to implement this interface.


protected:
	FTimerHandle LifeSpanTimer;
	virtual void Deactivate() = 0;
public:
	float LifeSpan;
	bool bIsActive;
	virtual void SetActive(bool Active) = 0;
};
