// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ObjectPoolActor.generated.h"

UCLASS()
class BIOCRISIS_API AObjectPoolActor : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AObjectPoolActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	UFUNCTION(BlueprintCallable)
		void Initiazlize();
	UPROPERTY(EditAnywhere)
		int PoolSize;
	UPROPERTY(EditAnywhere)
		float ActorLifeTime;
	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<AActor> PooledActorClass;
	TArray<AActor*> Pool;

public:
	UFUNCTION(BlueprintCallable)
		class AActor* GetAvailableActor();

};

