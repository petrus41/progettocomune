// Fill out your copyright notice in the Description page of Project Settings.


#include "StateMachineComponent.h"

// Sets default values for this component's properties
UStateMachineComponent::UStateMachineComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

}


// Called when the game starts
void UStateMachineComponent::BeginPlay()
{
	Super::BeginPlay();

	InitState();

}


// Called every frame
void UStateMachineComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (CurrentState == nullptr)
		return;
	if (bDebug)
		GEngine->AddOnScreenDebugMessage(-1, 0, FColor::Yellow, FString::Printf(TEXT(" %s"), *CurrentState->GetName()));
	CurrentState->StateTick();
}
void UStateMachineComponent::SwitchStateByKey(FString StateName)
{
	UStateBase* NextState = StateMap.FindRef(StateName);
	if (!NextState)
	{
		return;
	}
	if (!CurrentState)
	{
		CurrentState = NextState;
		CurrentState->OnStateEnter(GetOwner());
	}
	else if (!(CurrentState->GetClass() == NextState->GetClass() && !CurrentState->bCanRepeat))
	{
		CurrentState->OnStateExit();
		CurrentState = NextState;
		CurrentState->OnStateEnter(GetOwner());
	}
}

void UStateMachineComponent::InitStateManger()
{
	SwitchStateByKey(InizialState);
}

void UStateMachineComponent::InitState()
{
	for (const TPair<FString, TSubclassOf<UStateBase>> pair : PossibleState)
	{
		UStateBase* State = NewObject<UStateBase>(this, pair.Value);
		StateMap.Add(pair.Key, State);
	}
}