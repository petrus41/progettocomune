// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "StateBase.generated.h"

/**
 *
 */
UCLASS(Blueprintable)
class BIOCRISIS_API UStateBase : public UObject
{
	GENERATED_BODY()
public:
	UPROPERTY(EditDefaultsOnly)
		bool bCanTick;
	UPROPERTY(EditDefaultsOnly)
		bool bCanRepeat;
	UPROPERTY(EditDefaultsOnly)
		FName StateName;

	virtual void OnStateEnter(AActor* StateOwner);
	virtual void StateTick();
	virtual void OnStateExit();
};
