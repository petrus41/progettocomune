// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "StateBase.h"
#include "Components/ActorComponent.h"
#include "StateMachineComponent.generated.h"


UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class BIOCRISIS_API UStateMachineComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UStateMachineComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

public:

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
		FString InizialState;
	UPROPERTY(EditDefaultsOnly)
		TMap<FString, TSubclassOf<UStateBase>> PossibleState;
	UPROPERTY(BlueprintReadOnly)
		UStateBase* CurrentState;
	UPROPERTY(BlueprintReadOnly)
		TMap<FString, UStateBase*> StateMap;
	UPROPERTY(EditDefaultsOnly)
		bool bDebug;
	void SwitchStateByKey(FString StateName);
	void InitStateManger();
private:
	bool bCanTick;
	void InitState();
};

